# Hashbeat

Welcome to Hashbeat.

This beat is derived from lsbeat, adding the ability to include a hash (MD5 or SHA256) of the files. The intent is to provide the ability to detect changes to files. This could be for the purpose of file integrity checking or alerting on changes to critical system files, that shouldn’t normally change. For instance, an application configuration file.

The expectation is that this will be used for a relatively small number of files, each themselves of a small size.

Ensure that this folder is at the following location:
`${GOPATH}/gitlab.com/jdcockrill/hashbeat`

## Getting Started with Hashbeat

### Requirements

* [Golang](https://golang.org/dl/) 1.9 - created against 1.9. May work with earlier versions.
* Python
* virtualenv

### Init Project
To get running with Hashbeat and also install the
dependencies, run the following command:

```
make setup
```

It will create a clean git history for each major step. Note that you can always rewrite the history if you wish before pushing your changes.

To push Hashbeat in the git repository, run the following commands:

```
git remote set-url origin https://gitlab.com/jdcockrill/hashbeat
git push origin master
```

For further development, check out the [beat developer guide](https://www.elastic.co/guide/en/beats/libbeat/current/new-beat.html).

### Build

To build the binary for Hashbeat run the command below. This will generate a binary
in the same directory with the name hashbeat.

```
make
```


### Run

To run Hashbeat with debugging output enabled, run:

```
./hashbeat -c hashbeat.yml -e -d "*"
```


### Test

To test Hashbeat, run the following command:

```
make testsuite
```

alternatively:
```
make unit-tests
make system-tests
make integration-tests
make coverage-report
```

The test coverage is reported in the folder `./build/coverage/`

### Update

Each beat has a template for the mapping in elasticsearch and a documentation for the fields
which is automatically generated based on `_meta/fields.yml`.
To generate _meta/hashbeat.template.json and _meta/hashbeat.asciidoc

```
make update
```


### Cleanup

To clean  Hashbeat source code, run the following commands:

```
make fmt
make simplify
```

To clean up the build directory and generated artifacts, run:

```
make clean
```


### Clone

To clone Hashbeat from the git repository, run the following commands:

```
mkdir -p ${GOPATH}/gitlab.com/jdcockrill/hashbeat
cd ${GOPATH}/gitlab.com/jdcockrill/hashbeat
git clone https://gitlab.com/jdcockrill/hashbeat
```


For further development, check out the [beat developer guide](https://www.elastic.co/guide/en/beats/libbeat/current/new-beat.html).


## Packaging

The beat frameworks provides tools to crosscompile and package your beat for different platforms. This requires [docker](https://www.docker.com/) and vendoring as described above. To build packages of your beat, run the following command:

```
make package
```

This will fetch and create all images required for the build process. The hole process to finish can take several minutes.
