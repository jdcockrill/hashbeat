from hashbeat import BaseTest

import os


class Test(BaseTest):

    def test_base(self):
        """
        Basic test with exiting Hashbeat normally
        """
        self.render_config_template(
            path=os.path.abspath(self.working_dir) + "/log/*"
        )

        hashbeat_proc = self.start_beat()
        self.wait_until(lambda: self.log_contains("hashbeat is running"))
        exit_code = hashbeat_proc.kill_and_wait()
        assert exit_code == 0
