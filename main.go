package main

import (
	"os"

	"github.com/elastic/beats/libbeat/beat"

	"gitlab.com/jdcockrill/hashbeat/beater"
)

func main() {
	err := beat.Run("hashbeat", "", beater.New)
	if err != nil {
		os.Exit(1)
	}
}
