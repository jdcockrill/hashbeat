package beater

import (
	"fmt"
	"time"

	"hash"
	"crypto/md5"
	"crypto/sha256"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"

	"github.com/elastic/beats/libbeat/beat"
	"github.com/elastic/beats/libbeat/common"
	"github.com/elastic/beats/libbeat/logp"
	"github.com/elastic/beats/libbeat/publisher"

	"gitlab.com/jdcockrill/hashbeat/config"
)

type Hashbeat struct {
	done          chan struct{}
	config        config.Config
	client        publisher.Client
	lastIndexTime time.Time
}

// Creates beater
func New(b *beat.Beat, cfg *common.Config) (beat.Beater, error) {
	config := config.DefaultConfig
	if err := cfg.Unpack(&config); err != nil {
		return nil, fmt.Errorf("Error reading config file: %v", err)
	}

	bt := &Hashbeat{
		done:   make(chan struct{}),
		config: config,
	}
	return bt, nil
}

func (bt *Hashbeat) Run(b *beat.Beat) error {
	logp.Info("hashbeat is running! Hit CTRL-C to stop it.")
	bt.client = b.Publisher.Connect()
	ticker := time.NewTicker(bt.config.Period)
	for {
		now := time.Now()
		bt.hashFiles(bt.config.Path, b.Name) // call hashFiles
		bt.lastIndexTime = now               // mark Timestamp
		logp.Info("Event sent")
		select {
		case <-bt.done:
			return nil
		case <-ticker.C:
		}
	}
}

func (bt *Hashbeat) Stop() {
	bt.client.Close()
	close(bt.done)
}

func (bt *Hashbeat) hashFiles(dirFile string, beatname string) {
	// make this so that if it's a file or list of files, it can accept either
	// maybe add includes/excludes masks
	files, _ := ioutil.ReadDir(dirFile)
	for _, f := range files {
		t := f.ModTime()
		path := filepath.Join(dirFile, f.Name())
		if t.After(bt.lastIndexTime) {
			hash := ""
			if !f.IsDir() {
				switch bt.config.Hashtype {
				case "md5":
					hash = hashFile(path, md5.New())
				case "sha256":
					hash = hashFile(path, sha256.New())
				}
			}
			event := common.MapStr{
				"@timestamp": common.Time(time.Now()),
				"type":       beatname,
				"modtime":    common.Time(t),
				"filename":   f.Name(),
				"path":       path,
				"directory":  f.IsDir(),
				"filesize":   f.Size(),
				"hash":       hash,
				"hashtype":	bt.config.Hashtype,
			}
			bt.client.PublishEvent(event)
		}
		if f.IsDir() {
			bt.hashFiles(path, beatname)
		}
	}
}

func hashFile(fname string, h hash.Hash) string {
	f, err := os.Open(fname)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	if _, err := io.Copy(h, f); err != nil {
		log.Fatal(err)
	}

	return fmt.Sprintf("%x", h.Sum(nil))
}
